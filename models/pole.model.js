const mongoose = require('mongoose');

const poleSchema = mongoose.Schema({

    nomPole: {
        type: String,
        unique: true
    },
    idResponsable: {
        type: mongoose.Types.ObjectId,
        ref: 'user'
    },

}, {
    timestamps: true
})

const Pole = mongoose.model('pole', poleSchema);
module.exports = { Pole };