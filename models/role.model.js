const mongoose = require('mongoose');



const roleShema = mongoose.Schema({

    role: {
        type: String,
    },
    description: {
        type: String,
    },

}, {
    timestamps: true
})

const Role = mongoose.model('role', roleShema);
module.exports = { Role };