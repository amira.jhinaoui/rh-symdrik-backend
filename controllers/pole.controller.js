const { Pole } = require('../models/pole.model');
//const { User } = require('../models/user.model');


module.exports = {

    createPole: async (req, res, next) => {
        let results = {};
        const { nomPole, idResponsable } = req.body;

        const pole = new Pole(nomPole, idResponsable);
        await pole.save()
            .then((_pole) => {
                results.status = 200;
                results.result = _pole;
                return res.status(results.status).json(results);
            })
            .catch((err) => {
                results.status = 500;
                results.error = err.message;
                res.status(results.status).json(results);
            })


    },


}
