const { Role } = require('../models/role.model');


module.exports = {

    createRole: async (req, res, next) => {
        let results = {};
        const { role, description } = req.body;

        const _role = new Role(role, description);
        await _role.save()
            .then((__role) => {
                results.status = 200;
                results.result = __role;
                res.status(results.status).json(result);
            })
            .catch((err) => {
                results.status = 500;
                results.error = err.message;
                res.status(results.status).json(results);
            });


    },


    displayOneRole: async (req, res, next) => {
        let results = {};
        const { id } = req.params;

        await Role.findOne({ _id: id })
            .then((_role) => {
                results.status = 200;
                results.result = _role;
                res.status(results.status).json(results);
            })
            .catch((err) => {
                results.status = 500;
                results.error = err.message;
                res.status(results.status).json(results);
            });

    },

    displayAllRoles: async (req, res, next) => {
        let results = {};

        await Role.find()
            .then((roles) => {
                results.status = 200;
                results.result = roles;
                res.status(results.status).json(results);
            })
            .catch((err) => {
                results.status = 500;
                results.error = err.message;
                res.status(results.status).json(results);
            });

    },


    updateRoleById: async (req, res, next) => {
        let results = {};

        const { id } = req.params;
        const { role, description } = req.body;

        // check the existense of the required role
        const _role = await Role.findOne({ _id: id });

        if (!_role) {
            results.status = 404;
            results.error = "There is no role with this specific id !";
            res.status(results.status).json(results);
        } else {
            _role.role = role;
            _role.description = description;
            await _role.save()
                .then((__role) => {

                    results.status = 200;
                    results.result = __role;
                    res.status(results.status).json(results);
                })
                .catch((err) => {
                    results.status = 500;
                    results.error = err.message;
                    res.status(results.status).json(results);
                });
        }


    },



}
