const express = require('express');

const router = express.Router();
const poleController = require('../controllers/pole.controller');


/**
 * @path /poles
 */

router.post('/createPole', poleController.createPole);



module.exports = router;