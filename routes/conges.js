const express = require('express');

const router = express.Router();
const congeController = require('../controllers/conge.controller');


/**
 * @path /conges
 */

router.post('/createHoliday', congeController.createHoliday);
router.get('/displayAllHolidays', congeController.displayAllHolidays);
router.get('/displayOneHoliday/:id', congeController.displayOneHoliday);
router.put('/updateHolidayById/:id', congeController.updateHolidayById);


module.exports = router;
