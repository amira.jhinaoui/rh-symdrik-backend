const mongoose = require('mongoose');

const userSchema = mongoose.Schema({

    nom: {
        type: String
    },
    prenom: {
        type: String
    },
    dateNes: {
        type: Date,
        default: Date
    },
    dateEmbauche: {
        type: Date,
        default: Date.now
    },
    email: {
        type: String,
        unique: true
    },
    password: {
        type: String
    },
    avatar: {
        type: String
    },
    idRole: {
        type: mongoose.Types.ObjectId,
        ref: 'role'
    },
    idPole: {
        type: mongoose.Types.ObjectId,
        ref: 'pole'
    },

}, {
    timestamps: true
})

const User = mongoose.model('user', userSchema);
module.exports = { User };