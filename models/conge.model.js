const mongoose = require('mongoose');



const congeShema = mongoose.Schema({

    duree: {
        type: Number,
    },
    unite: {
        type: String,
    },
    description: {
        type: String,
    },

}, {
    timestamps: true
})

const Conge = mongoose.model('conge', congeShema);
module.exports = { Conge };