const express = require('express');

const router = express.Router();
const ferierController = require('../controllers/ferier.controller');


/**
 * @path /feriers
 */


router.post('/createPublicHoliday', ferierController.createPublicHoliday);
router.get('/displayAllPublicHoliday', ferierController.displayAllPublicHoliday);



module.exports = router;
