const { Ferier } = require('../models/ferier.model');


module.exports = {

    createPublicHoliday: async (req, res, next) => {
        let results = {};
        const { dateDebut, dateFin, fete, etat } = req.body;

        if (parseInt(dateDebut) > parseInt(dateFin)) {
            results.status = 400;
            results.error = "first date shouldn't be biger than the last date!"
            res.status(results.status).json(results);

        } else {
            const ferier = new Ferier(dateDebut, dateFin, fete, etat);
            await ferier.save()
                .then((ferier) => {
                    results.status = 200;
                    results.result = ferier;
                    return res.status(results.status).json(results);
                })
                .catch((err) => {
                    results.status = 500;
                    results.error = err.message;
                    res.status(results.status).json(results);
                })
        }

    },

    displayAllPublicHoliday: async (req, res, next) => {
        let results = {};

        await Ferier.find()
            .then((ferier) => {
                results.status = 200;
                results.result = ferier;
                res.status(results.status).json(results);
            })
            .catch((err) => {
                results.status = 500;
                results.error = err.message;
                res.status(results.status).json(results);
            })

    },



}
