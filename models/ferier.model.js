const mongoose = require('mongoose');



const ferierShema = mongoose.Schema({

    dateDebut: {
        type: Date,
        default: Date.now
    },
    dateFin: {
        type: Date,
        default: Date.now
    },
    fete: {
        type: String,
    },
    etat: {
        type: Boolean,
    },


}, {
    timestamps: true
})

const Ferier = mongoose.model('ferier', ferierShema);
module.exports = { Ferier };