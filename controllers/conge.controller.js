const { Conge } = require('../models/conge.model');


module.exports = {

    createHoliday: async (req, res, next) => {
        let results = {};
        const { duree, unite, description } = req.body;

        const _conge = new Conge(duree, unite, description);
        await _conge.save()
            .then((conge) => {
                results.status = 200;
                results.result = conge;
                res.status(results.status).json(results);
            })
            .catch((err) => {
                results.status = 500;
                results.error = err.message;
                res.status(results.status).json(results);
            });


    },


    displayOneHoliday: async (req, res, next) => {
        let results = {};
        const { id } = req.params;

        await Conge.findOne({ _id: id })
            .then((_conge) => {
                results.status = 200;
                results.result = _conge;
                res.status(results.status).json(results);
            })
            .catch((err) => {
                results.status = 500;
                results.error = err.message;
                res.status(results.status).json(results);
            });

    },

    displayAllHolidays: async (req, res, next) => {
        let results = {};

        await Conge.find()
            .then((conges) => {
                results.status = 200;
                results.result = conges;
                res.status(results.status).json(results);
            })
            .catch((err) => {
                results.status = 500;
                results.error = err.message;
                res.status(results.status).json(results);
            });

    },


    updateHolidayById: async (req, res, next) => {
        let results = {};

        const { id } = req.params;
        const { duree, unite, description } = req.body;

        // check the existense of the required role
        const _conge = await Conge.findOne({ _id: id });

        if (!_conge) {
            results.status = 404;
            results.error = "There is no holiday with this specific id !";
            res.status(results.status).json(results);
        } else {
            _conge.duree = duree;
            _conge.unite = unite;
            _conge.description = description;
            await _conge.save()
                .then((__conge) => {

                    results.status = 200;
                    results.result = __conge;
                    res.status(results.status).json(results);
                })
                .catch((err) => {
                    results.status = 500;
                    results.error = err.message;
                    res.status(results.status).json(results);
                });
        }


    },



}
