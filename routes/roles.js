const express = require('express');

const router = express.Router();
const roleController = require('../controllers/role.controller');


/**
 * @path /roles
 */

router.post('/createRole', roleController.createRole);
router.get('/displayAllRoles', roleController.displayAllRoles);
router.get('/displayOneRole/:id', roleController.displayOneRole);
router.put('/updateRoleById/:id', roleController.updateRoleById);

module.exports = router;
